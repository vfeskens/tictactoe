var boxes   = document.getElementsByTagName('input');
var playerName;
var defaultName = "noname";

window.onload = function() {
    initializeGame();

    for(var i=0; i<boxes.length; i++) {
        boxes[i].indeterminate = true;

        boxes[i].onclick = function(e) {
                var coordinates = determineCoordinates(this.id);
                disableBoxes();

                $.ajax({
                    url: "http://localhost:8080/rest/executeTurn",
                    data: {column: coordinates.x, row: coordinates.y},
                    type: "POST",
                    dataType: "json",
                    complete: function(data){
                        //console.log(data.responseJSON);
                        document.getElementById('currentScore').innerHTML = data.responseJSON.currentScore.numberOfMoves;
                        updateBoard(data);
                    },
                    error: function(){
                        //Do nothing for now
                    }
                 });
        }
    }

}

function disableBoxes() {
    for(var i=0; i<boxes.length; i++) {
        boxes[i].disabled = true;
    }
}

function enableIndeterminateBoxes() {
    for(var i=0; i<boxes.length; i++) {
        if(boxes[i].indeterminate == true) {
            boxes[i].disabled = false;
        }
    }
}

function updateBoard(response) {
    for(a = 0; a < 3; a++) {
        for(b = 0; b < 3; b++) {
            updatePosition(determineId(a,b), response.responseJSON.boardPositions[a][b]);
        }
    }
    if(response.responseJSON.result === 'PLAYER_WINNER') {
        showPrompt("you won!");
    } else if (response.responseJSON.result === 'COMPUTER_WINNER') {
        showPrompt("you lost!");
    } else if (response.responseJSON.result === 'DRAW') {
        showPrompt("draw!");
    }
    enableIndeterminateBoxes();
}

function updatePosition(id, player) {
    var box   = document.getElementById(id);
    if(player === 'X') {
        box.checked = true;
        box.disabled = true;
        box.indeterminate = false;
    } else if (player === 'O') {
        box.checked = false;
        box.disabled = true;
        box.indeterminate = false;
    } else {
        box.indeterminate = true;
        box.checked = false;
        box.disabled = false;
    }
}

function determineId(x, y) {
    if(x == 0) {
        switch(y) {
            case 0: return "c0";
            case 1: return "c3";
            case 2: return "c6";
        }
    } else if (x == 1) {
        switch(y) {
            case 0: return "c1";
            case 1: return "c4";
            case 2: return "c7";
        }
    } else if (x == 2) {
        switch(y) {
            case 0: return "c2";
            case 1: return "c5";
            case 2: return "c8";
        }
    }
}

function determineCoordinates(id) {
    switch (id) {
        case "c0": return {x:0, y:0};
        case "c1": return {x:1, y:0};
        case "c2": return {x:2, y:0};
        case "c3": return {x:0, y:1};
        case "c4": return {x:1, y:1};
        case "c5": return {x:2, y:1};
        case "c6": return {x:0, y:2};
        case "c7": return {x:1, y:2};
        case "c8": return {x:2, y:2};
        default: return "";
    }
}

function resetBoard() {
    for(a = 0; a < 3; a++) {
        for(b = 0; b < 3; b++) {
            updatePosition(determineId(a,b), null);
        }
    }
}

function initializeGame() {
    var playerName = prompt("What is your name?", defaultName);
    defaultName = playerName;

    $.ajax({
        url: "http://localhost:8080/rest/initialize",
        data: { playerName: playerName },
        type: "POST",
        dataType: "json",
        complete: function(data){
            updateBoard(data);
        },
        error: function(){
            //Do nothing for now
        }
     });
     resetBoard();
     document.getElementById('currentScore').innerHTML = "0";
}


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function showPrompt(message) {
    await sleep(100);
    alert(message);
    resetBoard();
    initializeGame();
}