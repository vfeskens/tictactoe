var app = angular.module('app', []);

app.service('MyService', function($http) {
    this.getHighScores = function() {
        return $http({
            method: 'GET',
            url: '/rest/highScores'
        });
    }
});

app.controller('HighScoresController', function ($scope, $interval, $location, MyService) {
    $scope.highScores = null;

    this.refresh = function() {
        MyService.getHighScores().then(function(dataResponse) {
            $scope.highScores = dataResponse.data;
        });

        console.log("Data refreshed " + new Date() + ".");
    }

    this.refresh();

    $interval(this.refresh, 1000);
});