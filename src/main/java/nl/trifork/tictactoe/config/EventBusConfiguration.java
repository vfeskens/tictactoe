package nl.trifork.tictactoe.config;

import com.google.common.eventbus.EventBus;
import nl.trifork.tictactoe.logic.ai.HumanMoveSubscriberService;
import nl.trifork.tictactoe.logic.score.ScoreService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventBusConfiguration {

    @Bean
    public EventBus eventBus(ScoreService scoreService, HumanMoveSubscriberService humanMoveSubscriberService) {
        EventBus eventBus = new EventBus();
        eventBus.register(scoreService);
        eventBus.register(humanMoveSubscriberService);
        return eventBus;
    }
}
