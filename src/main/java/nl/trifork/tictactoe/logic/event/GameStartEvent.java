package nl.trifork.tictactoe.logic.event;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import nl.trifork.tictactoe.logic.domain.Player;

@Builder
@Getter
@ToString
public class GameStartEvent {
    private Player playerOne;
    private Player playerTwo;
}
