package nl.trifork.tictactoe.logic.event;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import nl.trifork.tictactoe.logic.domain.Player;

@Builder
@Getter
@ToString
public class PlayerMoveEvent {
    private Character[][] boardState;
    private int x;
    private int y;
    private Player player;
}
