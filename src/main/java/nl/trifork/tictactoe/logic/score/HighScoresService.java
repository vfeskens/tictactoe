package nl.trifork.tictactoe.logic.score;

import lombok.extern.slf4j.Slf4j;
import nl.trifork.tictactoe.logic.domain.Score;
import nl.trifork.tictactoe.logic.score.scoresorter.ScoreSorter;
import nl.trifork.tictactoe.logic.score.scoresorter.ScoreSorterImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class HighScoresService {

    private static final int NUMBER_OF_SCORES_TO_KEEP = 10;

    private List<Score> scoresTable = new ArrayList<>();

    private ScoreSorter scoreSorter;

    public HighScoresService() {
        this.scoreSorter = new ScoreSorterImpl();
    }

    public void saveScore(Score score) {
        List<Score> currentScores = scoresTable;
        currentScores.add(score);

        //Sort scores, according to scoring sorting strategy
        List<Score> sortedScores = scoreSorter.sortScores(currentScores);

        //Keep (at most) the best 10 scores
        int size = sortedScores.size() < NUMBER_OF_SCORES_TO_KEEP ? sortedScores.size() : NUMBER_OF_SCORES_TO_KEEP;
        scoresTable = sortedScores.subList(0, size);
    }

    public List<Score> getHighScores() {
        return scoresTable;
    }
}
