package nl.trifork.tictactoe.logic.score.scoresorter;

import nl.trifork.tictactoe.logic.domain.Score;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ScoreSorterImpl implements ScoreSorter {

    public List<Score> sortScores(List<Score> inputScores) {

        if(inputScores == null) {
            throw new IllegalArgumentException("We cannot sort when input list is null.");
        }

        return inputScores.stream()
                .sorted(Comparator.comparing(Score::getNumberOfSteps).thenComparing(Score::getTimeInSeconds))
                .collect(Collectors.toList());
    }
}
