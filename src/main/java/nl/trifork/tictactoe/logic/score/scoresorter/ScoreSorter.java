package nl.trifork.tictactoe.logic.score.scoresorter;

import nl.trifork.tictactoe.logic.domain.Score;

import java.util.List;

public interface ScoreSorter {
    List<Score> sortScores(List<Score> inputScores);
}
