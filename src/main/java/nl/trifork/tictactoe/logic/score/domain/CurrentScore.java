package nl.trifork.tictactoe.logic.score.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrentScore {
    private int numberOfMoves;
    private Long startTime;

    public void incrementNumberOfMoves() {
        this.numberOfMoves++;
    }
}
