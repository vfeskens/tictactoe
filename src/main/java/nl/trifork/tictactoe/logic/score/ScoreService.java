package nl.trifork.tictactoe.logic.score;

import com.google.common.eventbus.Subscribe;
import nl.trifork.tictactoe.logic.domain.PlayerType;
import nl.trifork.tictactoe.logic.domain.Player;
import nl.trifork.tictactoe.logic.domain.Score;
import nl.trifork.tictactoe.logic.event.GameEndEvent;
import nl.trifork.tictactoe.logic.event.GameStartEvent;
import nl.trifork.tictactoe.logic.event.PlayerMoveEvent;
import nl.trifork.tictactoe.logic.score.domain.CurrentScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class ScoreService {

    private Map<Player, CurrentScore> playerScoreMap = new HashMap<>();

    @Autowired
    private HighScoresService highScoresService;

    @Subscribe
    public void handleGameStartEvent(GameStartEvent gameStartEvent) {
        playerScoreMap.clear();
        playerScoreMap.put(gameStartEvent.getPlayerOne(), new CurrentScore());
        playerScoreMap.put(gameStartEvent.getPlayerTwo(), new CurrentScore());
    }

    @Subscribe
    public void handlePlayerMoveEvent(PlayerMoveEvent playerMoveEvent) {
        CurrentScore currentScore = playerScoreMap.get(playerMoveEvent.getPlayer());

        if (currentScore == null) return;

        if (currentScore.getStartTime() == null) currentScore.setStartTime(System.currentTimeMillis());

        currentScore.incrementNumberOfMoves();
    }

    @Subscribe
    public void handleGameEndEvent(GameEndEvent gameEndEvent) {
        //If there is a winning player and this player is human, then save this player score.
        if (gameEndEvent.getWinningPlayer() != null && gameEndEvent.getWinningPlayer().getType().equals(PlayerType.PLAYER)) {
            playerScoreMap.entrySet().stream()
                    .filter(item -> item.getKey().equals(gameEndEvent.getWinningPlayer()))
                    .forEach(item -> {
                        highScoresService.saveScore(Score.builder()
                                .numberOfSteps(item.getValue().getNumberOfMoves())
                                .timeInSeconds(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - item.getValue().getStartTime()))
                                .playerName(item.getKey().getName())
                                .build());
                    });
        }
    }

    public Optional<CurrentScore> determineHumanPlayerScore() {
        return playerScoreMap.entrySet().stream()
                .filter(item -> item.getKey().getType().equals(PlayerType.PLAYER))
                .map(Map.Entry::getValue)
                .findFirst();
    }
}
