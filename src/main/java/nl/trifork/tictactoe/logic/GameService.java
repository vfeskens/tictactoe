package nl.trifork.tictactoe.logic;

import lombok.extern.slf4j.Slf4j;
import nl.trifork.tictactoe.logic.domain.GameResponse;
import nl.trifork.tictactoe.logic.domain.Player;
import nl.trifork.tictactoe.logic.domain.PlayerType;
import nl.trifork.tictactoe.logic.score.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class GameService {

    private GameState gameState;

    private ScoreService scoreService;

    @Autowired
    public GameService(GameState gameState, ScoreService scoreService) {
        this.gameState = gameState;
        this.scoreService = scoreService;
    }

    public GameResponse executeMove(int x, int y) {
        log.info("Executing move x: {} y: {}", x, y);

        gameState.executeMove(x, y);

        GameResponse gameResponse =  gameState.constructGameResponse();

        scoreService.determineHumanPlayerScore().ifPresent(gameResponse::setCurrentScore);

        return gameResponse;
    }



    public GameResponse initializeGame(String playerName) {
        log.info("Initializing new game, for player : {}", playerName);

        //For now we always play with one human player and one computer player.
        Player playerOne = Player.builder().type(PlayerType.COMPUTER).name("Vinnie").build();
        Player playerTwo = Player.builder().type(PlayerType.PLAYER).name(playerName).build();

        List<Player> players = Arrays.asList(playerOne, playerTwo);
        Collections.shuffle(players);

        players.get(0).setGameLetter('X');
        players.get(1).setGameLetter('O');

        gameState.initializeGame(players);

        return gameState.constructGameResponse();
    }
}
