package nl.trifork.tictactoe.logic.ai.movedeterminer;

import nl.trifork.tictactoe.logic.ai.HumanMoveSubscriberService;
import nl.trifork.tictactoe.logic.domain.MoveCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

import static java.util.Collections.reverseOrder;

@Component
public class HumanLikeMoveDeterminer implements MoveDeterminer {

    private HumanMoveSubscriberService humanMoveSubscriberService;

    @Autowired
    public HumanLikeMoveDeterminer(HumanMoveSubscriberService humanMoveSubscriberService) {
        this.humanMoveSubscriberService = humanMoveSubscriberService;
    }

    @Override
    public Optional<MoveCommand> determineMove(Character[][] boardPositions) {
        Map<MoveCommand, Integer> possibleMoves = humanMoveSubscriberService.determineMoves(boardPositions);
        return possibleMoves
                .entrySet()
                .stream()
                .sorted(reverseOrder(Map.Entry.comparingByValue()))
                .map(Map.Entry::getKey)
                .findFirst();
    }


}
