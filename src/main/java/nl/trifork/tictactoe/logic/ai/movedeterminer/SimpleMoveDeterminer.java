package nl.trifork.tictactoe.logic.ai.movedeterminer;

import nl.trifork.tictactoe.logic.domain.MoveCommand;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
public class SimpleMoveDeterminer implements MoveDeterminer {

    private Random random = new Random();

    @Override
    public Optional<MoveCommand> determineMove(Character[][] boardPositions) {
        List<MoveCommand> availableMoves = determineAvailableMoves(boardPositions);
        int randomNumber = random.nextInt(availableMoves.size());
        return Optional.of(MoveCommand.builder()
                .x(availableMoves.get(randomNumber).getX())
                .y(availableMoves.get(randomNumber).getY())
                .build());
    }


    private List<MoveCommand> determineAvailableMoves(Character[][] boardPositions) {
        List<MoveCommand> availableMoves = new ArrayList<>();

        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                if (boardPositions[a][b] == null) {
                    availableMoves.add(new MoveCommand(a, b));
                }
            }
        }

        return availableMoves;
    }
}
