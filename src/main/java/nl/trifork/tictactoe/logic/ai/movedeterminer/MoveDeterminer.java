package nl.trifork.tictactoe.logic.ai.movedeterminer;

import nl.trifork.tictactoe.logic.domain.MoveCommand;

import java.util.Optional;

public interface MoveDeterminer {

    Optional<MoveCommand> determineMove(Character[][] boardPositions);
}
