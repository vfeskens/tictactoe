package nl.trifork.tictactoe.logic.ai.movedeterminer;

import nl.trifork.tictactoe.logic.domain.MoveCommand;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class FirstOpenPositionDeterminer implements MoveDeterminer {
    @Override
    public Optional<MoveCommand> determineMove(Character[][] boardPositions) {
        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                if (boardPositions[a][b] == null) {
                    return Optional.of(MoveCommand.builder().x(a).y(b).build());
                }
            }
        }
        return Optional.empty();
    }
}
