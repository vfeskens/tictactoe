package nl.trifork.tictactoe.logic.ai.domain;

import lombok.Builder;
import lombok.Getter;
import nl.trifork.tictactoe.logic.domain.MoveCommand;

@Builder
@Getter
public class HumanMove {
    private String boardPositions;
    private MoveCommand moveCommand;
}
