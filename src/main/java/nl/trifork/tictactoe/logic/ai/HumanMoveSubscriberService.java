package nl.trifork.tictactoe.logic.ai;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;
import nl.trifork.tictactoe.logic.ai.domain.HumanMove;
import nl.trifork.tictactoe.logic.domain.PlayerType;
import nl.trifork.tictactoe.logic.domain.MoveCommand;
import nl.trifork.tictactoe.logic.event.PlayerMoveEvent;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@Slf4j
public class HumanMoveSubscriberService {

    private Map<HumanMove, Integer> recordedHumanMoves = new HashMap<>();

    @Subscribe
    public void handlePlayerMoveEvent(PlayerMoveEvent event) {
        log.info("Incoming PlayerMoveEvent : {}", event);
        if(event.getPlayer().getType().equals(PlayerType.PLAYER)) {
            recordHumanMove(event.getBoardState(), MoveCommand.builder().x(event.getX()).y(event.getY()).build());
        }
    }

    private void recordHumanMove(Character[][] boardPositions, MoveCommand moveCommand) {
        String stringRepresentationOfBoardPositions = Arrays.deepToString(boardPositions);
        HumanMove move = HumanMove.builder().moveCommand(moveCommand).boardPositions(stringRepresentationOfBoardPositions).build();
        recordedHumanMoves.compute(move, (key, oldValue) -> oldValue == null ? 1 : oldValue + 1);
    }

    public Map<MoveCommand, Integer> determineMoves(Character[][] boardPositions) {
        String stringRepresentationOfBoardPositions = Arrays.deepToString(boardPositions);
        return recordedHumanMoves.entrySet().stream()
                .filter(item -> item.getKey().getBoardPositions().equals(stringRepresentationOfBoardPositions))
                .collect(Collectors.toMap(entry -> entry.getKey().getMoveCommand(), Map.Entry::getValue));
    }
}
