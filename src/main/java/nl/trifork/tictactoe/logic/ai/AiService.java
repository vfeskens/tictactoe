package nl.trifork.tictactoe.logic.ai;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;
import nl.trifork.tictactoe.logic.GameService;
import nl.trifork.tictactoe.logic.ai.movedeterminer.MoveDeterminer;
import nl.trifork.tictactoe.logic.domain.PlayerType;
import nl.trifork.tictactoe.logic.domain.MoveCommand;
import nl.trifork.tictactoe.logic.event.TurnAnnounceEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Service
@Slf4j
public class AiService {

    private GameService gameService;

    private MoveDeterminer humanLikeMoveDeterminer;

    private MoveDeterminer simpleMoveDeterminer;

    @Autowired
    public AiService(GameService gameService, MoveDeterminer humanLikeMoveDeterminer, MoveDeterminer simpleMoveDeterminer, EventBus eventBus) {
        this.gameService = gameService;
        this.humanLikeMoveDeterminer = humanLikeMoveDeterminer;
        this.simpleMoveDeterminer = simpleMoveDeterminer;
        eventBus.register(this);
    }

    @Subscribe
    public void handleTurnAnnounceEvent(TurnAnnounceEvent turnAnnounceEvent) {
        log.info("Incoming TurnAnnounceEvent : {}", turnAnnounceEvent);
        if (turnAnnounceEvent.getPlayer().getType().equals(PlayerType.COMPUTER)) {

            MoveCommand moveCommand =
                    Stream.<Supplier<Optional<MoveCommand>>>of(
                            () -> humanLikeMoveDeterminer.determineMove(turnAnnounceEvent.getBoardState()),
                            () -> simpleMoveDeterminer.determineMove(turnAnnounceEvent.getBoardState()))
                            .map(Supplier::get)
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .findFirst().orElseThrow(() -> new RuntimeException("Unable to determine move for computer to play!"));

            gameService.executeMove(moveCommand.getX(), moveCommand.getY());
        }
    }
}
