package nl.trifork.tictactoe.logic;

import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.eventbus.EventBus;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import nl.trifork.tictactoe.logic.domain.GameResponse;
import nl.trifork.tictactoe.logic.domain.Player;
import nl.trifork.tictactoe.logic.domain.PlayerType;
import nl.trifork.tictactoe.logic.event.GameEndEvent;
import nl.trifork.tictactoe.logic.event.GameStartEvent;
import nl.trifork.tictactoe.logic.event.PlayerMoveEvent;
import nl.trifork.tictactoe.logic.event.TurnAnnounceEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Getter
@Component
public class GameState {

    private List<Player> players;

    private Character[][] boardState;

    private Player turn;

    private Iterator<Player> playerIterator;

    private EventBus eventBus;

    @Autowired
    public GameState(EventBus eventBus) {
        this.eventBus = eventBus;
        resetGameState();
    }

    public void initializeGame(List<Player> players) {
        //Reset game
        resetGameState();

        //Initialize players + iterator
        this.players = Arrays.asList(players.get(0), players.get(1));
        playerIterator = Iterators.cycle(players);

        //Announce gamestartevent + turn announce event, let the game begin!
        turn = playerIterator.next();
        eventBus.post(GameStartEvent.builder().playerOne(turn).playerTwo(players.get(1)).build());
        eventBus.post(TurnAnnounceEvent.builder().player(turn).boardState(boardState).build());
    }

    public void executeMove(int x, int y) {
        //Publish move event (interesting for the score service and the human move subscriber service (for the computer to learn from human moves))
        eventBus.post(PlayerMoveEvent.builder().x(x).y(y).player(turn).boardState(boardState).build());

        //Adjust boardState
        boardState[x][y] = turn.getGameLetter();

        //Next player's turn
        this.turn = playerIterator.next();

        //Either we publish game end event or turn announce event.
        if(determineCurrentResult().equals(GameResponse.Result.TO_BE_DETERMINED)) {
            eventBus.post(TurnAnnounceEvent.builder().boardState(boardState).player(turn).build());
        } else {
            eventBus.post(GameEndEvent.builder().winningPlayer(determineWinner()).build());
        }
    }

    public GameResponse constructGameResponse() {
        return GameResponse.builder()
                .boardPositions(boardState)
                .result(determineCurrentResult())
                .winner(determineWinner())
                .build();
    }

    private void resetGameState() {
        this.players = Collections.emptyList();
        this.boardState = new Character[3][3];
    }

    private boolean movesLeft() {
        List<Character> positionsFilled = Arrays.stream(boardState).flatMap(Arrays::stream)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return positionsFilled.size() < 9;
    }

    private Player determineWinner() {
        if(areTheSameAndNotNull(boardState[0][0], boardState[0][1], boardState[0][2])) {
            return determineWinner(boardState[0][0]);
        } else if (areTheSameAndNotNull(boardState[1][0], boardState[1][1], boardState[1][2])) {
            return determineWinner(boardState[1][0]);
        } else if (areTheSameAndNotNull(boardState[2][0], boardState[2][1], boardState[2][2])) {
            return determineWinner(boardState[2][0]);
        } else if (areTheSameAndNotNull(boardState[0][0], boardState[1][0], boardState[2][0])) {
            return determineWinner(boardState[0][0]);
        } else if (areTheSameAndNotNull(boardState[0][1], boardState[1][1], boardState[2][1])) {
            return determineWinner(boardState[0][1]);
        } else if (areTheSameAndNotNull(boardState[0][2], boardState[1][2], boardState[2][2])) {
            return determineWinner(boardState[0][2]);
        } else if (areTheSameAndNotNull(boardState[0][0], boardState[1][1], boardState[2][2])) {
            return determineWinner(boardState[0][0]);
        } else if (areTheSameAndNotNull(boardState[2][0], boardState[1][1], boardState[0][2])) {
            return determineWinner(boardState[2][0]);
        }
        return null;
    }

    private boolean areTheSameAndNotNull(Character a, Character b, Character c) {
        return a != null && a.equals(b) && a.equals(c);
    }

    private Player determineWinner(Character character) {
        return players.stream()
                .filter(item -> character.equals(item.getGameLetter()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("This shouldn't happen, we cannot determine the winner!"));
    }

    private GameResponse.Result determineCurrentResult() {

        Player player = determineWinner();

        if(player == null) {
            return movesLeft() ? GameResponse.Result.TO_BE_DETERMINED : GameResponse.Result.DRAW;
        } else {
            switch (player.getType()) {
                case PLAYER: return GameResponse.Result.PLAYER_WINNER;
                case COMPUTER: return GameResponse.Result.COMPUTER_WINNER;
            }
        }

        return GameResponse.Result.TO_BE_DETERMINED;
    }


}
