package nl.trifork.tictactoe.logic;

import lombok.extern.slf4j.Slf4j;
import nl.trifork.tictactoe.logic.domain.GameResponse;
import nl.trifork.tictactoe.logic.domain.Score;
import nl.trifork.tictactoe.logic.score.HighScoresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/rest")
public class RestLogicController {

    @Autowired
    private GameService gameService;

    @Autowired
    private HighScoresService highScoresService;


    @RequestMapping(value = "/executeTurn", method = RequestMethod.POST)
    public GameResponse move(@RequestParam int column, @RequestParam int row) {
        log.info("Move incoming x: {} and y: {}", column, row);
        return gameService.executeMove(column, row);
    }

    @RequestMapping(value = "/initialize", method = RequestMethod.POST)
    public GameResponse initialize(@RequestParam String playerName) {
        return gameService.initializeGame(playerName);
    }

    @RequestMapping(value = "/highScores", method = RequestMethod.GET)
    public List<Score> getHighScores() {
        return highScoresService.getHighScores();
    }
}
