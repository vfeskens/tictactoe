package nl.trifork.tictactoe.logic.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import nl.trifork.tictactoe.logic.score.domain.CurrentScore;

@Builder
@Getter
@Setter
public class GameResponse {
    private Character[][] boardPositions;
    private Result result;
    private Player winner;
    private CurrentScore currentScore;

    public enum Result {
        PLAYER_WINNER,
        COMPUTER_WINNER,
        DRAW,
        TO_BE_DETERMINED
    }
}


