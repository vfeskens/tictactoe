package nl.trifork.tictactoe.logic.domain;

public enum PlayerType {
    PLAYER,
    COMPUTER
}
