package nl.trifork.tictactoe.logic.domain;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Data
@Builder
public class MoveCommand {
    private int x;
    private int y;
}
