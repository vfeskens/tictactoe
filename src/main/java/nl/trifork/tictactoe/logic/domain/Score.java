package nl.trifork.tictactoe.logic.domain;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Score {
    private Integer numberOfSteps;
    private Long timeInSeconds;
    private String playerName;
}
