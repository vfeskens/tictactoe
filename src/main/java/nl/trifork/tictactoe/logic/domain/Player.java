package nl.trifork.tictactoe.logic.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
public class Player {
    private String name;
    private PlayerType type;
    private char gameLetter;
}
