package nl.trifork.tictactoe.logic.ai.movedeterminer;

import nl.trifork.tictactoe.logic.ai.movedeterminer.MoveDeterminer;
import nl.trifork.tictactoe.logic.ai.movedeterminer.SimpleMoveDeterminer;
import nl.trifork.tictactoe.logic.domain.MoveCommand;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class SimpleMoveDeterminerTest {

    private MoveDeterminer moveDeterminer = new SimpleMoveDeterminer();

    @Test
    public void shouldReturnValidMove() {
        //Given
        Character[][] filledPositions = new Character[3][3];
        filledPositions[0][0] = 'a';
        filledPositions[0][1] = 'a';
        filledPositions[0][2] = 'a';
        filledPositions[1][0] = 'a';
        filledPositions[1][1] = 'a';
        filledPositions[1][2] = 'a';
        filledPositions[2][0] = 'a';
        filledPositions[2][1] = 'a';

        //When
        Optional<MoveCommand> moveCommand = moveDeterminer.determineMove(filledPositions);

        //Then
        assertTrue(moveCommand.isPresent());
        assertEquals(new MoveCommand(2, 2), moveCommand.get());
    }
}