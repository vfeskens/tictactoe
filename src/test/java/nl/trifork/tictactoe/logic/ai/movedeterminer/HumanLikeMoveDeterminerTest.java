package nl.trifork.tictactoe.logic.ai.movedeterminer;

import nl.trifork.tictactoe.logic.ai.HumanMoveSubscriberService;
import nl.trifork.tictactoe.logic.domain.MoveCommand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HumanLikeMoveDeterminerTest {

    @Mock
    private HumanMoveSubscriberService humanMoveSubscriberService;

    @InjectMocks
    private MoveDeterminer moveDeterminer = new HumanLikeMoveDeterminer(humanMoveSubscriberService);

    @Test
    public void shouldReturnValidMove() {
        //Given
        MoveCommand moveCommand1 = MoveCommand.builder().x(0).y(0).build();
        MoveCommand moveCommand2 = MoveCommand.builder().x(1).y(2).build();

        Map<MoveCommand, Integer> moveCommandIntegerMap = new HashMap<>();
        moveCommandIntegerMap.put(moveCommand1, 2);
        moveCommandIntegerMap.put(moveCommand2, 10);

        when(humanMoveSubscriberService.determineMoves(any())).thenReturn(moveCommandIntegerMap);

        Character[][] filledPositions = new Character[3][3];
        filledPositions[0][1] = 'a';
        filledPositions[0][2] = 'a';
        filledPositions[1][0] = 'a';
        filledPositions[1][1] = 'a';
        filledPositions[2][0] = 'a';
        filledPositions[2][1] = 'a';

        //When
        Optional<MoveCommand> moveCommand = moveDeterminer.determineMove(filledPositions);

        //Then
        assertTrue(moveCommand.isPresent());
        assertEquals(new MoveCommand(1, 2), moveCommand.get());
    }

    @Test
    public void shouldNotReturnAMove() {
        //Given
        Map<MoveCommand, Integer> moveCommandIntegerMap = new HashMap<>();

        when(humanMoveSubscriberService.determineMoves(any())).thenReturn(moveCommandIntegerMap);

        Character[][] filledPositions = new Character[3][3];
        filledPositions[0][1] = 'a';
        filledPositions[0][2] = 'a';
        filledPositions[1][0] = 'a';
        filledPositions[1][1] = 'a';
        filledPositions[1][2] = 'a';
        filledPositions[2][0] = 'a';
        filledPositions[2][1] = 'a';

        //When
        Optional<MoveCommand> moveCommand = moveDeterminer.determineMove(filledPositions);

        //Then
        assertFalse(moveCommand.isPresent());
    }
}