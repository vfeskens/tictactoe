package nl.trifork.tictactoe.logic.ai.movedeterminer;

import nl.trifork.tictactoe.logic.domain.MoveCommand;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;


public class FirstOpenPositionDeterminerTest {
    private MoveDeterminer moveDeterminer = new FirstOpenPositionDeterminer();

    @Test
    public void shouldReturnValidMove() {
        //Given
        Character[][] filledPositions = new Character[3][3];
        filledPositions[0][0] = 'a';
        filledPositions[0][1] = 'a';
        filledPositions[0][2] = 'a';
        filledPositions[1][1] = 'a';
        filledPositions[1][2] = 'a';
        filledPositions[2][0] = 'a';
        filledPositions[2][1] = 'a';

        //When
        Optional<MoveCommand> moveCommand = moveDeterminer.determineMove(filledPositions);

        //Then
        assertTrue(moveCommand.isPresent());
        assertEquals(new MoveCommand(1, 0), moveCommand.get());
    }
}