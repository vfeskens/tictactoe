package nl.trifork.tictactoe.logic;

import com.google.common.eventbus.EventBus;
import nl.trifork.tictactoe.logic.domain.GameResponse;
import nl.trifork.tictactoe.logic.domain.Player;
import nl.trifork.tictactoe.logic.domain.PlayerType;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;


public class GameStateTest {


    @Test
    public void shouldDeclarePlayerWinner() {
        //Given
        Player playerOne = Player.builder().name("Vince").type(PlayerType.PLAYER).gameLetter('x').build();
        Player playerTwo = Player.builder().name("Ronald").type(PlayerType.PLAYER).gameLetter('o').build();
        GameState gameState = new GameState(new EventBus());
        gameState.initializeGame(Arrays.asList(playerOne, playerTwo));

        //When
        gameState.executeMove(0, 0);
        gameState.executeMove(1, 1);
        gameState.executeMove(0, 1);
        gameState.executeMove(2, 2);
        gameState.executeMove(0, 2);

        //Then
        assertEquals(playerOne, gameState.constructGameResponse().getWinner());

    }

    @Test
    public void shouldBeDraw() {
        //Given
        Player playerOne = Player.builder().name("Vince").type(PlayerType.PLAYER).gameLetter('x').build();
        Player playerTwo = Player.builder().name("Ronald").type(PlayerType.PLAYER).gameLetter('o').build();
        GameState gameState = new GameState(new EventBus());
        gameState.initializeGame(Arrays.asList(playerOne, playerTwo));

        //When
        gameState.executeMove(0,0);
        gameState.executeMove(0,2);
        gameState.executeMove(0,1);
        gameState.executeMove(1,0);
        gameState.executeMove(1,2);
        gameState.executeMove(1,1);
        gameState.executeMove(2,0);
        gameState.executeMove(2,1);
        gameState.executeMove(2,2);

        //Then
        assertEquals(GameResponse.Result.DRAW, gameState.constructGameResponse().getResult());
    }
}