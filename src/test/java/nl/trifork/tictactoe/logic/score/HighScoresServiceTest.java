package nl.trifork.tictactoe.logic.score;

import nl.trifork.tictactoe.logic.domain.Score;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class HighScoresServiceTest {
    private HighScoresService highScoresService = new HighScoresService();

    @Test
    public void shouldOnlySaveBest10Scores(){
        //Given
        Score bestScore = Score.builder().playerName("Paul").timeInSeconds(10L).numberOfSteps(1).build();
        highScoresService.saveScore(bestScore);
        highScoresService.saveScore(bestScore);
        highScoresService.saveScore(bestScore);
        highScoresService.saveScore(bestScore);
        highScoresService.saveScore(bestScore);

        highScoresService.saveScore(Score.builder().playerName("Paul").timeInSeconds(10L).numberOfSteps(2).build());
        highScoresService.saveScore(Score.builder().playerName("Paul").timeInSeconds(10L).numberOfSteps(3).build());
        highScoresService.saveScore(Score.builder().playerName("Paul").timeInSeconds(10L).numberOfSteps(4).build());
        highScoresService.saveScore(Score.builder().playerName("Paul").timeInSeconds(10L).numberOfSteps(5).build());
        highScoresService.saveScore(Score.builder().playerName("Paul").timeInSeconds(10L).numberOfSteps(6).build());

        Score reallyBad = Score.builder().playerName("Paul").timeInSeconds(1L).numberOfSteps(7).build();
        highScoresService.saveScore(reallyBad);

        //When
        List<Score> highScores = highScoresService.getHighScores();

        //Then
        assertEquals(10, highScores.size());
        assertFalse(highScores.contains(reallyBad));
        assertEquals(bestScore, highScores.get(0));
    }
}