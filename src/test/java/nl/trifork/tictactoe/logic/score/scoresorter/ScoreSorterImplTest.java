package nl.trifork.tictactoe.logic.score.scoresorter;

import nl.trifork.tictactoe.logic.domain.Score;
import nl.trifork.tictactoe.logic.score.scoresorter.ScoreSorter;
import nl.trifork.tictactoe.logic.score.scoresorter.ScoreSorterImpl;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ScoreSorterImplTest {

    private ScoreSorter scoreSorter = new ScoreSorterImpl();

    @Test
    public void shouldSortScoresCorrectly() throws Exception {
        //Given
        Score score1 = Score.builder().numberOfSteps(4).timeInSeconds(5L).build();
        Score score2 = Score.builder().numberOfSteps(3).timeInSeconds(1L).build();
        Score score3 = Score.builder().numberOfSteps(3).timeInSeconds(5L).build();

        //When
        List<Score> sortedScores = scoreSorter.sortScores(Arrays.asList(score1, score2, score3));

        //Then
        assertEquals(Arrays.asList(score2, score3, score1), sortedScores);
    }

    @Test
    public void shouldBeAbleToWorkWithEmptyList() {
        //Given
        List<Score> inputList = Collections.emptyList();

        //When
        List<Score> outputList = scoreSorter.sortScores(inputList);

        //Then
        assertEquals(Collections.emptyList(), outputList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenNull() {
        //Given
        List<Score> inputList = null;

        //When
        scoreSorter.sortScores(inputList);
    }

}